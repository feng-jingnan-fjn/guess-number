import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

    /**
     * 猜数字游戏
     */
    public static void guess(){
        Random r = new Random();
        int num = r.nextInt(100)+1;
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < 8; i++) {
            System.out.println("请输入你猜的数字：");
            int gessnum = sc.nextInt();
            if(i==7){
                if(gessnum==num){
                    System.out.println("恭喜你，第7次终于猜对啦！");
                }
                else{
                    System.out.println("很遗憾！次数用完");
                    System.out.println("正确答案是："+num);
                    break;
                }
            }
            if(gessnum>num){
                System.out.println("你猜得太大了！请重试");
            }
            else if(gessnum<num){
                System.out.println("你猜得太小了！请重试");
            }
            else {
                System.out.println("恭喜你，你猜对啦！");
                System.out.println("你一共猜了"+i+"次，继续加油！");
                break;
            }
        }
        System.out.println("游戏结束。");
    }


    public static void main(String[] args) {
        guess();
        while (true){
            System.out.println("请选择是否再次游戏！");
            System.out.println("1-是  2-否");
            Scanner sc = new Scanner(System.in);
            int num = sc.nextInt();
            if (num==1){
                guess();
            }else {
                return;
            }
        }

    }
}
