# GuessNumber

#### 介绍
Java用户猜数字游戏开源项目
（1）实现随机生成1-100之间的一个整型数作为答案并保存的功能.
    （2）实现从命令行打印提示信息并接受用户猜测数字的功能.
    （3）实现比较答案数字与用户猜测数字关系后打印提示信息的功能.
    （4）实现在玩家测错情况下可以重复不停猜测直至猜对的功能.
    （5）实现限制玩家只可以猜测7次，若7次依然未猜对，游戏结束。
    （6）实现一轮


1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
